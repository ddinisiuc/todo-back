<?php

namespace TODO\v1\Repositories;

use TODO\v1\Repositories\Interfaces\BoardInterface;

class BoardRepository implements BoardInterface
{
    public function get($repository)
    {
        return $repository->findAll();
    }

    public function getBoardsData($boards): array
    {
        $boardData = [];

        foreach ($boards as $board) {
            $boardData[] = [
                'id' => $board->getId(),
                'name' => $board->getName(),
                'description' => $board->getDescription(),
                'status' => $board->getStatus(),
                'slug' => $board->getSlug()
            ];
        }

        return $boardData;
    }

    public function getSteps($board): array
    {
        $stepArray = [];

        foreach($board->getSteps() as $step) {
            $stepArray[] = [
                'id' => $step->getId(),
                'name' => $step->getName(),
            ];
        }

        return $stepArray;
    }

    public function getTasks($board)
    {
        $taskArray = [];
        foreach ($board->getTasks() as $task) {
            $taskArray[] = [
                'id' => $task->getId(),
                'name' => $task->getName(),
                'description' => $task->getDescription(),
                'status' => $task->getStatus()
            ];
        }

        return $taskArray;
    }
}