<?php

namespace TODO\v1\Repositories\Interfaces;

interface BoardInterface
{
    public function getBoardsData($boards);
    public function getSteps($board);
    public function getTasks($board);
}