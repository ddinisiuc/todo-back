<?php

$routes = [
    'GET' => [
        '/v1/boards/{board_id}/todos' => 'TodoController@index',
        '/v1/boards' => 'BoardController@index',
        '/v1/boards/{board_slug}' => 'BoardController@show',
    ],
    'POST' => [
        '/v1/boards/{board_id}/todos' => 'TodoController@create',
        '/v1/boards' => 'BoardController@create',
    ],
    'PUT' => [
        '/v1/boards/{board_id}/todos/{todo_id}' => 'TodoController@edit',
    ],
    'DELETE' => [
        '/v1/boards/{board_id}/todos/{todo_id}' => 'TodoController@delete',
    ]
];