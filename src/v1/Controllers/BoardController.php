<?php

namespace TODO\v1\Controllers;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use TODO\v1\Models\Board;
use TODO\v1\Repositories\Interfaces\BoardInterface;

class BoardController
{
    private EntityManager $entityManager;
    private BoardInterface $boardRepository;
    public function __construct(EntityManager $entityManager, BoardInterface $boardRepository)
    {
        $this->entityManager = $entityManager;
        $this->boardRepository = $boardRepository;
    }

    public function index()
    {
        $repository = $this->entityManager->getRepository(Board::class);
        $boards = $this->boardRepository->get($repository);
        $boardData = $this->boardRepository->getBoardsData($boards);

        http_response_code(200);
        header('Content-Type: application/json');

        echo json_encode(['data' => [
            'boards' => $boardData
        ]]);
    }

    public function show($board_slug)
    {
        $repository = $this->entityManager->getRepository(Board::class);
        $board = $repository->findOneBy(['slug' => $board_slug]);

        if(!$board) {
            http_response_code(404);
            echo json_encode([
                "error" => [
                    "code" => 404,
                    "message" => "Resource not found",
                ]
            ]);
            return;
        }
        $stepsArray = $this->boardRepository->getSteps($board);


        $taskArray = $this->boardRepository->getTasks($board);


        http_response_code(200);
        header('Content-Type: application/json');

        echo json_encode(['data' => [
            'board' => [
                'id' => $board->getId(),
                'slug' => $board->getSlug(),
                'description' => $board->getDescription(),
                'tasks' => $taskArray,
                'steps' => $stepsArray
            ]
        ]]);
    }

    public function create(Request $request)
    {
        $board = new Board();
        $board->setName($request->query->get('name'));
        $board->setDescription($request->query->get('description'));
        $board->setSlug($board->getName());
        $board->setStatus('Open');

        try {
            $this->entityManager->persist($board);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            //TODO logic for unique slug and create board
            header('Content-Type: application/json');

            echo json_encode([
                "error" => [
                    "code" => $exception->getCode(),
                    "message" => $exception->getMessage(),
                ]
            ]);
            return;
        }

        header('Content-Type: application/json');

        echo json_encode(['data' => [
            'message' => 'Task created successfully',
            'board' => [
                'id' => $board->getId(),
                'name' => $board->getName(),
                'description' => $board->getDescription(),
                'slug' => $board->getSlug(),
                'status' => $board->getStatus()
            ]
        ]]);
    }
}