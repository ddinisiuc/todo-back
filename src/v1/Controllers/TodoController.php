<?php

namespace TODO\v1\Controllers;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use TODO\v1\Models\Board;
use TODO\v1\Models\Task;

class TodoController
{
    private EntityManager $entityManager;
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function index($board_id)
    {
        $board = $this->entityManager->find(Board::class, $board_id);

        if(!$board) {
            http_response_code(404);
            header('Content-Type: application/json');

            echo json_encode([
                "error" => [
                    "code" => 404,
                    "message" => "Resource not found",
                ]
            ]);
            return;
        }
        $taskArray = [];
        foreach ($board->getTasks() as $task) {
            $taskArray[] = [
                'id' => $task->getId(),
                'name' => $task->getName(),
                'description' => $task->getDescription(),
            ];
        }
        header('Content-Type: application/json');

        echo json_encode(['data' => [
            'board' => [
                'name' => $board->getName(),
                'tasks' => $taskArray
            ]
        ]]);
    }

    public function create($board_id, Request $request)
    {
        //TODO add validation
        $task = new Task();
        $task->setName($request->query->get('name'));
        $task->setDescription($request->get('description'));

        $board = $this->entityManager->find(Board::class, $board_id);

        if (!$board) {
            // Board not found, return a 404 response
            http_response_code(404);
            header('Content-Type: application/json');

            echo json_encode([
                "error" => [
                    "code" => 404,
                    "message" => "Resource not found",
                ]
            ]);
            return;
        }

        $task->setBoard($board);
        //todo:set default first step

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        http_response_code(201);
        header('Content-Type: application/json');

        echo json_encode([
            'message' => 'Task created successfully',
            'task' => [
                'id' => $task->getId(),
                'name' => $task->getName(),
                'description' => $task->getDescription(),
            ]
        ]);
    }

    public function edit($board_id, $task_id, Request $request)
    {
        $board = $this->entityManager->find(Board::class, $board_id);

        if (!$board) {
            // Board not found, return a 404 response
            http_response_code(404);
            header('Content-Type: application/json');

            echo json_encode([
                "error" => [
                    "code" => 404,
                    "message" => "Resource not found",
                ]
            ]);
            return;
        }


        $task = $this->entityManager->find(Task::class, $task_id);

        if(!$task && $task->getBoard()->getId() == $board_id) {
            http_response_code(404);
            header('Content-Type: application/json');

            echo json_encode([
                "error" => [
                    "code" => 404,
                    "message" => "Task not found",
                ]
            ]);
            return;
        }

        $data = json_decode($request->getContent(), true);

        if(isset($data['name'])) {
            $task->setName($data['name']);
        }

        if(isset($data['description'])) {
            $task->setName($data['description']);
        }

        if (isset($data['status'])) {
            $task->setStatus($data['status']);
        }
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        http_response_code(201);
        header('Content-Type: application/json');

        echo json_encode([
            'message' => 'Task updated successfully',
            'task' => [
                'id' => $task->getId(),
                'name' => $task->getName(),
                'description' => $task->getDescription(),
                'status' => $task->getStatus()
            ]
        ]);

    }

    public function delete($board_id, $task_id)
    {
        $board = $this->entityManager->find(Board::class, $board_id);

        if (!$board) {
            // Board not found, return a 404 response
            http_response_code(404);
            header('Content-Type: application/json');

            echo json_encode([
                "error" => [
                    "code" => 404,
                    "message" => "Resource not found",
                ]
            ]);
            return;
        }

        $task = $board->findTask($task_id);

        if(!$task) {
            http_response_code(404);
            header('Content-Type: application/json');

            echo json_encode([
                "error" => [
                    "code" => 404,
                    "message" => "Resource not found",
                ]
            ]);
            return;
        }

        $this->entityManager->remove($task);
        $this->entityManager->flush();

        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode(['message' => 'Task deleted successfully']);
    }
}