<?php

namespace TODO\v1\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'steps')]
class Step
{
    #[ORM\Id]
    #[ORM\Column]
    #[ORM\GeneratedValue]
    private int $id;
    #[ORM\Column(length: 50)]
    private string $name;
    #[ORM\Column]
    private bool $status;

    #[ORM\ManyToMany(targetEntity: Board::class)]
    #[ORM\JoinTable(name: 'board_step')]
    #[ORM\JoinColumn('step_id', 'id')]
    #[ORM\InverseJoinColumn('board_id', 'id')]
    private Collection $boards;

    public function __construct()
    {
        $this->boards = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
    public function getSlug()
    {
        return $this->slug;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getStatus()
    {
        return $this->status;
    }
}