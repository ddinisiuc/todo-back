<?php

namespace TODO\v1\Models;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'boards')]
class Board
{
    #[ORM\Id]
    #[ORM\Column]
    #[ORM\GeneratedValue]
    private int $id;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(length: 500)]
    private string $slug;

    #[ORM\Column(length: 5000)]
    private string $description;

    #[ORM\Column]
    private bool $status;

    #[ORM\OneToMany(mappedBy: 'board', targetEntity: Task::class)]
//    #[ORM\JoinColumn(name: 'board_id')]
    public Collection $tasks;

    #[ORM\ManyToMany(targetEntity: Step::class)]
    #[ORM\JoinTable(name: 'board_step')]
    #[ORM\JoinColumn('board_id', 'id')]
    #[ORM\InverseJoinColumn('step_id', 'id')]
    private Collection $steps;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
        $this->steps = new ArrayCollection();
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setSlug(string $slug)
    {
        $slug = preg_replace('/[^A-Za-z0-9\s-]/', '', $slug);
        // Convert string to lowercase
        $slug = strtolower($slug);
        // Replace spaces with hyphens
        $slug = str_replace(' ', '-', $slug);
        // Remove multiple consecutive hyphens
        $slug = preg_replace('/-+/', '-', $slug);
        // Trim leading and trailing hyphens
        $slug = trim($slug, '-');

        $this->slug = $slug;
    }

    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    public function setDescription(string $description = '')
    {
        $this->description = $description;
    }

    public function getTasks()
    {
        return $this->tasks->getValues();
    }

    public function getSteps()
    {
        return $this->steps->getValues();
    }

    public function getId()
    {
        return $this->id;
    }
    public function getSlug()
    {
        return $this->slug;
    }
    public function getName()
    {
      return $this->name;
    }
    public function getDescription()
    {
      return $this->description ?? '';
    }
    public function getStatus()
    {
      return $this->status;
    }

    public function findTask(int $task_id)
    {
        foreach ($this->getTasks() as $task) {
            if($task->getId() === $task_id) {
                return $task;
            }
            return null;
        }
    }
}