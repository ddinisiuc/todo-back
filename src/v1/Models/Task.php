<?php

namespace TODO\v1\Models;

use Doctrine\ORM\Mapping as ORM;
#[ORM\Entity]
#[ORM\Table(name: 'tasks')]
class Task
{
    #[ORM\Id]
    #[ORM\Column]
    #[ORM\GeneratedValue]
    private int $id;

    #[ORM\Column(length: 255)]
    private string $name;
    #[ORM\Column(length: 1000)]
    private string $description;
    #[ORM\Column]
    private int $step_id;
    #[ORM\ManyToOne(targetEntity: Board::class, inversedBy: 'tasks')]
    #[ORM\JoinColumn(name: 'board_id', referencedColumnName: 'id')]
    public Board $board;

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setDescription(string $description = ''): void
    {
        $this->description = $description;
    }

    public function setStatus(int $step_id): void
    {
        $this->step_id = $step_id;
    }

    public function setBoard(Board $board): void
    {
        $this->board = $board;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    public function getStatus(): int
    {
        return $this->step_id ?? 0;
    }

    public function getBoard(): Board
    {
        return $this->board;
    }
}