<?php

use Symfony\Component\HttpFoundation\Request;

// Enable error reporting
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    http_response_code(200);
    exit();
}
define('ROOT_PATH', dirname(__DIR__, 2));

require_once ROOT_PATH . '/vendor/autoload.php';
$app = require_once ROOT_PATH . '/src/core/bootstrap.php';

$request = Request::createFromGlobals();
$requestMethod = $request->getMethod();
$requestUri = $request->getPathInfo();

// Define routes
$routes = [];

// Include the routes file
//TODO replace v1 with config
$routesFilePath = ROOT_PATH . '/src/v1/routes/api.php';

if (file_exists($routesFilePath)) {
    require_once $routesFilePath;
} else {
    // Routes file not found, log or handle the error accordingly
    http_response_code(500);
    echo '500 Internal Server Error: Routes file not found';
    exit;
}

// Match the requested route
$routeParams = null;
if (isset($routes[$requestMethod])) {
    foreach ($routes[$requestMethod] as $routePattern => $controllerAction) {
        // Convert route pattern to a regular expression
        $pattern = preg_replace('/\{.*?\}/', '([^\/]+)', $routePattern);
        $pattern = str_replace('/', '\/', $pattern);

        // Check if the request URI matches the route pattern
        if (preg_match('~^' . $pattern . '/?$~', $requestUri, $matches)) {
            // Extract route parameters
            $routeParams = array_slice($matches, 1);
            $controllerActionParts = explode('@', $controllerAction);
            $controllerClass = 'TODO\\v1\\Controllers\\' . $controllerActionParts[0];
            $actionMethod = $controllerActionParts[1];
            break;
        }
    }
}
// If a matching route was found, call the corresponding controller action
if ($routeParams !== null) {
    // Instantiate the controller class
    $controllerInstance = $app->get($controllerClass);
    // Call the controller action and pass request obj
    $routeParams[] = $request;
    call_user_func_array([$controllerInstance, $actionMethod], $routeParams);
} else {
    // No matching route found, handle 404 Not Found error
    http_response_code(404);
    echo '404 Not Found';
}