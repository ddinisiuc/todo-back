<?php

return [
    'database' => [
        'dbname' => 'todo',
        'user' => 'root',
        'password' => 'root',
        'host' => 'localhost',
        'port' => 3306,
        'driver' => 'pdo_mysql',
    ]
];
