<?php

namespace TODO\core;

class ConfigurationRepository
{
    protected array $config = [];

    public function __construct(string $configPath)
    {
        // Load configuration values from PHP files in the specified directory
        $this->loadConfigFiles($configPath);
    }

    protected function loadConfigFiles(string $configPath): void
    {
        // Get all PHP files in the directory
        $configFiles = glob($configPath . '/*.php');

        foreach ($configFiles as $file) {
            $config = require $file;
            if (is_array($config)) {
                $this->config = array_merge_recursive($this->config, $config);
            }
        }
    }

    public function get(string $key, $default = null)
    {
        // Return the value corresponding to the given key, or a default value if not found
        return $this->config[$key] ?? $default;
    }
}
