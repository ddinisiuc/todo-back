<?php

use DI\ContainerBuilder;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use TODO\core\ConfigurationRepository;
use TODO\v1\Repositories\BoardRepository;
use TODO\v1\Repositories\Interfaces\BoardInterface;


$containerBuilder = new ContainerBuilder();
$container = $containerBuilder->build();

$configRepository = new ConfigurationRepository(ROOT_PATH . '/src/config');
$container->set(ConfigurationRepository::class, function () use ($configRepository) {
    return $configRepository;
});

$container->set(EntityManager::class, function () use ($container){
    $dbConfig = $container->get(ConfigurationRepository::class)->get('database');
    $config = ORMSetup::createAttributeMetadataConfiguration(
        paths: array(ROOT_PATH . "/src/v1/Models"),
        isDevMode: true,
    );

    // Database connection configuration for MySQL
    $connectionParams = [
        'dbname' => $dbConfig['dbname'],
        'user' => $dbConfig['user'],
        'password' => $dbConfig['password'],
        'host' => $dbConfig['host'], // Change this if your MySQL server is on a different host
        'port' => $dbConfig['port'], // Change this if your MySQL server is running on a different port
        'driver' => $dbConfig['driver'],
    ];

    $connection = DriverManager::getConnection($connectionParams, $config);

    return new EntityManager($connection, $config);
    });

$container->set(BoardInterface::class, function () use ($container) {
    return $container->get(BoardRepository::class);
});

return $container;
