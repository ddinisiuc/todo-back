-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: todo
-- ------------------------------------------------------
-- Server version	8.0.36-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `board_step`
--

DROP TABLE IF EXISTS `board_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `board_step` (
  `id` int NOT NULL AUTO_INCREMENT,
  `step_id` int DEFAULT NULL,
  `board_id` int DEFAULT NULL,
  `order` smallint DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `board_step_pk` (`step_id`,`board_id`),
  KEY `board_step_boards_id_fk` (`board_id`),
  CONSTRAINT `board_step_board_steps_id_fk` FOREIGN KEY (`step_id`) REFERENCES `steps` (`id`),
  CONSTRAINT `board_step_boards_id_fk` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `board_step`
--

LOCK TABLES `board_step` WRITE;
/*!40000 ALTER TABLE `board_step` DISABLE KEYS */;
INSERT INTO `board_step` (`id`, `step_id`, `board_id`, `order`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,1,1,1,1,NULL,NULL,NULL),(4,2,1,2,1,NULL,'2024-05-17 20:52:53',NULL),(5,3,1,4,1,NULL,'2024-05-17 20:53:45',NULL),(6,4,1,3,1,NULL,'2024-05-17 20:52:53',NULL),(7,1,2,1,1,NULL,'2024-05-17 20:52:53',NULL),(9,2,2,2,1,NULL,'2024-05-17 20:54:21',NULL);
/*!40000 ALTER TABLE `board_step` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boards`
--

DROP TABLE IF EXISTS `boards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `boards` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `boards_pk` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boards`
--

LOCK TABLES `boards` WRITE;
/*!40000 ALTER TABLE `boards` DISABLE KEYS */;
INSERT INTO `boards` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES (1,'General','General todo list','1','2024-05-15 02:49:42','2024-05-16 13:53:52',NULL,'general'),(2,'Test',NULL,'1',NULL,'2024-05-16 13:53:52',NULL,'test'),(4,'Family','Tasks for my family','1',NULL,NULL,NULL,'family'),(10,'Family1','Tasks for my family','1',NULL,NULL,NULL,'family1'),(14,'Cactus',NULL,'1','2024-05-17 20:51:00',NULL,NULL,'cactus');
/*!40000 ALTER TABLE `boards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `steps`
--

DROP TABLE IF EXISTS `steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `steps` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Each todo list can have different steps';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `steps`
--

LOCK TABLES `steps` WRITE;
/*!40000 ALTER TABLE `steps` DISABLE KEYS */;
INSERT INTO `steps` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,'Open',1,'2024-05-15 02:50:02','2024-05-15 02:50:05',NULL),(2,'In Progress',1,NULL,NULL,NULL),(3,'Done',1,NULL,NULL,NULL),(4,'Review',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tasks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `step_id` int DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `board_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_boards_id_fk` (`board_id`),
  CONSTRAINT `tasks_boards_id_fk` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` (`id`, `name`, `step_id`, `description`, `board_id`) VALUES (21,'Add your name in the body',1,'some description for cv',1),(22,'Orizon',1,'some description for cv',1),(23,'Walcom',4,'some description for cv',1),(24,'leam',2,'some description for cv',2);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-26 22:26:59
