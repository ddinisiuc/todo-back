# Project installation

### Requirements
 - php 8.0
 - mysql 8.0
 - composer 2.2

### Local
For installation all dependencies and libraries, run
```
composer install
```
 To start localhost server run command:
 ```
 php -S localhost:8000 src/public/index.php
 ```
Entry point for applications is located `src/public/index.php`

### Others
In the `src/core/bootstrap.php` file you can find DI container, with
bindings,  register new ones e.t.c 

### Database
Database is located in root directory.

**Note!**
````
In order for the project to work correctly, you need to run a database dump
````
